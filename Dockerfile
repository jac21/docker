FROM maven:3.8.1-openjdk-17-slim

RUN mvn -f /home/app/pom.xml clean package


FROM maven:3.8.1-openjdk-17-slim
COPY --from=build /home/app/target/ar-0.0.1-SNAPSHOT.jar /usr/local/lib/ar.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/ar.jar"]